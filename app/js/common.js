let ip = localStorage.ip || 'localhost';
let port = parseInt(localStorage.port) || 2020;

var server = `http://${ip}:${port}`;
var socket = null;

window.onload = () => {
  let socketio = server + '/socket.io/socket.io.js';

  let script = document.createElement('script');
  script.src = socketio;
  script.onload = () => {
    socket = io.connect(server);
    socket.on('requestData', (arr)=>{
      let send = {};
      for (let i = 0; i < arr.length; i++) {
        send[arr[i]] = self[arr[i]];
      }

      console.log(send);
      socket.emit('data', send);
    })

    socket.on('login', (data)=>{
      console.log('login');
    })

    socket.on('update', (data)=>{
      console.log('login');
    })
  }

  document.body.appendChild(script);
};
