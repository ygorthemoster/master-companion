window.onload = function() {
  var conDialog = document.getElementById('connect');
  var hstDialog = document.getElementById('host');

  document.getElementById('conn').addEventListener('click', (ev) => {conDialog.showModal();});
  document.getElementById('hst').addEventListener('click', (ev) => {hstDialog.showModal();});

  let closeBtn = document.getElementsByClassName('close');
  for (var i = 0; i < closeBtn.length; i++) {
    closeBtn[i].addEventListener('click', (ev) =>{
      let d = ev.target;
      while(d.nodeName != 'DIALOG') d = d.parentNode;
      d.close();
    });
  }

  document.getElementById('conf').addEventListener('click', (ev) => {

  });

  document.getElementById('ip').addEventListener('input', (ev) =>{

    let curLength = ev.target.value.length;
    let curSelected = ev.target.selectionStart;
    let octets = ev.target.value.replace(/[^0-9\.]/g, '').split('.');

    for(i = 0; i < octets.length; i++){
      if(octets[i].length > 3){
        octets[i + 1] = octets[i].substring(3) + (octets[i + 1] || '');
        octets[i] = octets[i].substring(0, 3)
      }
    }

    let fullText = octets[0];
    for (var i = 1; i < 4; i++) {
      if(octets[i] != undefined)
        fullText += '.' + octets[i]
    }

    ev.target.value = fullText;

    let newLength = ev.target.value.length;
    let newSelected = newLength - curLength;
    newSelected += curSelected;

    ev.target.selectionStart = newSelected;
    ev.target.selectionEnd = newSelected;
  })

  document.getElementById('portc').addEventListener('input', (ev) =>{
    ev.target.value = ev.target.value.replace(/[^0-9]/g, '').substring(0, 5);
  });

  document.getElementById('porth').addEventListener('input', (ev) =>{
    ev.target.value = ev.target.value.replace(/[^0-9]/g, '').substring(0, 5);
  });

  document.getElementById('create').addEventListener('click', (ev) =>{
    let port = document.getElementById('porth').value;

    localStorage.ip = 'localhost';
    localStorage.port = port || 2020;
    
    nw.Window.open('server/console.html',{
      id:'console'
    }, () =>{
      nw.Window.get().close();
    });
  });

  document.getElementById('join').addEventListener('click', (ev) =>{

  });
};
