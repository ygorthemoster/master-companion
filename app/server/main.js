const express = require('express');
const path = require('path');

logger(`creating server on port ${localStorage.port}`);
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

// app.use('/GUI/style', express.static(path.join(process.cwd(), '/css')));
// app.use('/GUI/function', express.static(path.join(process.cwd(), '/js')));
// app.use('/GUI', express.static(path.join(process.cwd(), '/html')));

logger(`starting server on port ${localStorage.port}`);
server.listen(parseInt(localStorage.port));
logger(`listening on port ${localStorage.port}`);

logger(`setting up socket.io calls`);
var clients = {};
io.on('connect', function(socket){
  logger(`client ${socket.id} connected`);
  socket.emit('requestData', ['name', 'role']);

  socket.on('data', (data) =>{
    logger(`data recieved from ${socket.id}`);
    console.log(clients[socket.id] != 'undefined');

    if(typeof(clients[socket.id]) != 'undefined') {
      let name = false
      let role = false

      if(data.name){
        clients[socket.id].name = data.name;
        name = true;

        logger(`client ${socket.id} changed name to ${clients[socket.id].name}`);
      }

      if(data.role){
        clients[socket.id].role = data.role;
        role = true;

        logger(`client ${socket.id} changed role to ${clients[socket.id].role}`);
      }

      io.broadcast.emit('update',
        {
          id: socket.id,
          name: name ? clients[socket.id].name : undefined,
          role: role ? clients[socket.id].role : undefined
        });
    } else {
      clients[socket.id] = data
      logger(`client ${clients[socket.id].name} - ${clients[socket.id].role} (${socket.id}) logged in`);

      let send = data;
      send.id = socket.id

      io.broadcast.emit('login', send);
    }
  });

  socket.on('chat', (data) =>{

  });
});

logger(`opening master's window`);
nw.Window.open('/html/master.html', {

}, (win) => {
  logger(`openned master's window`);
  win.focus();
});
